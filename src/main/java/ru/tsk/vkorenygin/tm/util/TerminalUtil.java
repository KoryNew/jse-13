package ru.tsk.vkorenygin.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static int nextNumber() {
        final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

}
