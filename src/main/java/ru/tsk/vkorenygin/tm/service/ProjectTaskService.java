package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectTaskService;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty())
            return null;
        if (taskId == null || taskId.isEmpty())
            return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId))
            return null;
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty())
            return null;
        if (taskId == null || taskId.isEmpty())
            return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId))
            return null;
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String id) {
        if (id == null || id.isEmpty())
            return null;
        if (!projectRepository.existsById(id))
            return null;
        return taskRepository.findAllByProjectId(id);
    }

    @Override
    public Project removeProjectById(final String id) {
        if (id == null || id.isEmpty())
            return null;
        if (!projectRepository.existsById(id))
            return null;
        taskRepository.removeAllTaskByProjectId(id);
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeProjectByIndex(final int index) {
        if (index < 0)
            return null;
        final Project project = projectRepository.findByIndex(index);
        if (project == null)
            return null;
        taskRepository.removeAllTaskByProjectId(project.getId());
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        final Project project = projectRepository.findByName(name);
        if (project == null)
            return null;
        taskRepository.removeAllTaskByProjectId(project.getId());
        return projectRepository.removeByName(name);
    }

}
