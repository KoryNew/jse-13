package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty())
            return false;
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        return projectRepository.existsByIndex(index);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0)
            return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty())
            return null;
        if (status == null)
            return null;
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty())
            return null;
        if (status == null)
            return null;
        return projectRepository.changeStatusByName(name, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0)
            return null;
        if (status == null)
            return null;
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index<0)
            return null;
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index<0)
            return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0)
            return null;
        if (name == null || name.isEmpty())
            return null;
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty())
            return null;
        if (name == null || name.isEmpty())
            return null;
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void remove(Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
