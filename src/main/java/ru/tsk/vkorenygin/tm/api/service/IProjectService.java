package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<Project> findAll();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final Integer index);

    Project changeStatusById(final String id, final Status status);

    Project changeStatusByName(final String name, final Status status);

    Project changeStatusByIndex(final Integer index, final Status status);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    void remove(Project project);

    void clear();

}
