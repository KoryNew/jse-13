package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<Task> findAll();

    Task findById(String Id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task changeStatusById(final String id, final Status status);

    Task changeStatusByName(final String name, final Status status);

    Task changeStatusByIndex(final Integer index, final Status status);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    void remove(Task task);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    void clear();

}
