package ru.tsk.vkorenygin.tm.api.controller;

import ru.tsk.vkorenygin.tm.model.Task;

public interface ITaskController {

    void create();

    void show(final Task task);

    void showById();

    void showByIndex();

    void showByName();

    void showAll();

    void changeStatusById();

    void changeStatusByName();

    void changeStatusByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void updateById();

    void updateByIndex();

    void removeById();

    void removeByIndex();

    void removeByName();

    void clear();
}
