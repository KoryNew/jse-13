package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public boolean existsById(String id) {
        final Task task = findById(id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(Integer index) {
        if (index == null || index < 0)
            return false;
        return index < tasks.size();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findById(final String id) {
        for (Task task: tasks) {
            if (id.equals(task.getId()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task: tasks) {
            if (name.equals(task.getName()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task startById(String id) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String id) {
        final Task task = findById(id);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String id) {
        final List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (id.equals(task.getProjectId())) taskList.add(task);
        }
        if (taskList.size() == 0)
            return null;
        return taskList;
    }

    @Override
    public void removeAllTaskByProjectId(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getProjectId())) task.setProjectId(null);
        }
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
