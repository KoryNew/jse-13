package ru.tsk.vkorenygin.tm.controller;

import ru.tsk.vkorenygin.tm.api.controller.IProjectController;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void create() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showAll() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project: projects)
            System.out.println(project);
        System.out.println("[OK]");
    }

    public void show(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @Override
    public void showById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(project);
    }

    @Override
    public void showByName(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(project);
    }

    @Override
    public void showByIndex(){
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(project);
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void changeStatusByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void startById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void startByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void finishById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) System.out.println("Incorrect value");
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            System.out.println("[Incorrect value]");
            return;
        }

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();

        if (projectService.updateByIndex(index, name, description) == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Updated project]");
    }

    @Override
    public void updateById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id)) {
            System.out.println("[Incorrect value]");
            return;
        }

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();

        if (projectService.updateById(id, name, description) == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Updated project]");
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
